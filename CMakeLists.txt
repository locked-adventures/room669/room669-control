cmake_minimum_required(VERSION 3.5)

set(PROJ "room669-control")

project(${PROJ} LANGUAGES CXX)

find_package(PahoMqttCpp REQUIRED paho-mqttpp3)
find_package(Boost REQUIRED system)
find_package(Threads REQUIRED Threads)
find_package(PkgConfig REQUIRED)
find_package(nlohmann_json 3.2.0 REQUIRED)
find_package(Eigen3 REQUIRED NO_MODULE)

pkg_check_modules(SNDFILE REQUIRED IMPORTED_TARGET sndfile)
pkg_check_modules(PORTAUDIO REQUIRED IMPORTED_TARGET portaudiocpp)
pkg_check_modules(OLA REQUIRED libola)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)

#add_subdirectory(digio)

add_executable(${PROJ}
    main.cpp
    roomcontrol.cpp
    roomcontrol.hpp
    digio_client_repo.cpp
    digio/client.hpp
    digio/circ_buffer.hpp
    digio/client_impl.hpp
    multiaudio/async_player.hpp
    multiaudio/async_player.cpp
    date.h
)

target_link_libraries(${PROJ} PRIVATE
    PahoMqttCpp::paho-mqttpp3
    Boost::system
    Threads::Threads
    ${OLA_LIBRARIES}
    nlohmann_json::nlohmann_json
    PkgConfig::PORTAUDIO
    Eigen3::Eigen
    PkgConfig::SNDFILE
)

target_include_directories(${PROJ} PRIVATE ${OLA_INCLUDE_DIRS})
target_compile_options(${PROJ} PRIVATE ${OLA_CFLAGS_OTHER})

include(GNUInstallDirs)

configure_file("${PROJ}.service.in" "${PROJ}.service")

install(TARGETS "${PROJ}" DESTINATION "${CMAKE_INSTALL_FULL_BINDIR}")
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${PROJ}.service" DESTINATION "lib/systemd/system")
install(FILES "udev-rules/99-serial.rules" DESTINATION "lib/udev/rules.d")
