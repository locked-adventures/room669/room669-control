#ifndef CIRC_BUFFER_HPP
#define CIRC_BUFFER_HPP

#include <cinttypes>
#include <string>

template <std::size_t size>
class circ_buffer {
    static constexpr std::size_t actual_size = size + 1;
    char data[size + 1];
    std::size_t start;
    std::size_t end;

public:
    explicit
    circ_buffer()
        : data{}, start(0), end(0)
    {}

    void operator <<(char c) {
        data[end] = c;
        end = (end + 1) % actual_size;
        // In case of a buffer overflow the first characters get lost
        if (end == start) {
            start = (start + 1) % actual_size;
        }
        data[end] = '\0';
    }

    bool operator ==(const char *str) {
        using std::size_t;
        std::size_t i = start;
        for (; i != end && str; i = (i + 1) % actual_size, str++) {
            if (data[i] != *str) {
                return false;
            }
        }
        return i == end && *str == '\0';
    }

    bool operator ==(const std::string &str) {
        return operator ==(str.c_str());
    }
};

#endif // CIRC_BUFFER_HPP
