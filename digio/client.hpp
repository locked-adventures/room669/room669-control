#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <boost/asio/serial_port.hpp>
#include <boost/asio/any_io_executor.hpp>
#include <boost/system/error_code.hpp>
#include <string>
#include <utility>
#include <memory>
#include <functional>
#include <boost/asio/buffer.hpp>
#include <vector>
#include <exception>

#include "circ_buffer.hpp"

#include <atomic>

namespace digio {

namespace _impl {
    constexpr const char syn_word[] = "BEGIN\r\n";
    using std::atomic_bool;
}

struct exception : std::exception {
    const char *what() const noexcept {
        return "digio exception";
    }
};

struct callback
{
    // If an exception is thrown in init_callback or input_pin_callback, it will be handled by error_callback.
    // If an exception is thrown in error_callback, it will propagate throw the asio event loop.
    virtual void on_error(const std::exception &) {}

    // In this callback, the initial values of the Arduino pins can be read
    virtual void on_init() {}

    // The value of an input pin changed
    virtual void on_input_change(int /*pin*/, int /*value*/) {}

    // The value of an output pin changed
    virtual void on_output_change(int /*pin*/, int /*value*/) {}
};

template <typename Executor = boost::asio::any_io_executor>
class client
{
public:
    typedef Executor executor_type;

    std::vector<int> input_pins;
    std::vector<int> output_pins;

private:
    typedef circ_buffer<sizeof(_impl::syn_word) - 1> syn_buffer;

    std::shared_ptr<std::atomic_bool> failed;
    callback *cb;
    char c;
    std::unique_ptr<syn_buffer> syn;

    std::string buff_string;
    boost::asio::dynamic_string_buffer<char, std::char_traits<char>, std::allocator<char>> buff;

    boost::asio::basic_serial_port<Executor> serial;

    // Asynchronous operation callbacks

    void syn_read_callback(const boost::system::error_code& error, std::size_t bytes_transferred);

    void init_read_callback(const boost::system::error_code& error, std::size_t bytes_transferred);

    void read_callback(const boost::system::error_code& error, std::size_t bytes_transferred);

    void write_callback(const boost::system::error_code &error, std::size_t size, int pin, int value);


    void handle_exception(const std::exception &e);


    // Start asynchronous read operation

    void async_read_syn();

    void async_read_header();

    void async_read_event();

public:
    /* const input_pins_t& get_input_pins() {
        return input_pins;
    } */

    void cancel();

    void close();

    /*
    template <typename ExecutionContext>
    client(ExecutionContext &execution_context)
        : serial(execution_context)
    {}

    template <typename ExecutionContext>
    client(ExecutionContext &execution_context, const char *device)
        : serial(execution_context, device)
    {}

    template <typename ExecutionContext>
    client(ExecutionContext &execution_context, const std::string &device)
        : serial(execution_context, device)
    {}

    client(const executor_type &executor)
        : serial(executor)
    {}

    client(const executor_type &executor, const char *device)
        : serial(executor, device)
    {}

    client(const executor_type &executor, const std::string &device)
        : serial(executor, device)
    {}
    */

    explicit
    client(boost::asio::basic_serial_port<executor_type> &&serial, callback *cb = nullptr)
        : failed(std::make_shared<std::atomic_bool>(false))
        , cb(cb)
        , buff_string()
        , buff(buff_string)
        , input_pins()
        , serial(std::move(serial))
    {}

    void set_callback(callback *cb) {
        this->cb = cb;
    }

    callback *get_callback() {
        return cb;
    }

    void digital_write(int pin, bool value);

    void async_init();

    ~client() {
        *failed = true;
        // Serial port will be closed automatically
    }
};

}

#endif // CLIENT_HPP
