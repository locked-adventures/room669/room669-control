#ifndef CLIENT_IMPL_HPP
#define CLIENT_IMPL_HPP

#include "client.hpp"
#include <boost/asio/write.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/system/system_error.hpp>
#include <cinttypes>
#include <boost/bind/bind.hpp>
#include <cassert>

#include <iostream>

#include <nlohmann/json.hpp>

#include <sstream>

#include <string_view>
#include <iomanip>

namespace digio {

namespace _impl {

using boost::asio::async_read;
using boost::asio::async_read_until;
using boost::asio::async_write;
using boost::asio::buffer;
using boost::asio::const_buffer;
using boost::system::error_code;
using boost::system::system_error;
using std::size_t;
using std::make_unique;
using std::move;
using std::shared_ptr;
using std::make_shared;
using boost::bind;
using namespace boost::placeholders;

using std::cout;
using std::cerr;
using std::endl;

using std::vector;
using std::string;
using nlohmann::json;

using std::quoted;
using std::string_view;

using std::ostringstream;
using std::to_string;
}

template<typename Executor>
void client<Executor>::handle_exception(const std::exception &e)
{
    // Release some memory
    syn.reset();
    buff_string.clear();

    // Set failed flag
    *failed = true;

    // Cancel any other ongoing operations
    cancel();

    // Call error callback
    if (cb) cb->on_error(e);
}

template<typename Executor>
void client<Executor>::async_read_syn()
{
    using namespace _impl;

    shared_ptr<atomic_bool> failed_ref = failed;
    async_read(serial, buffer(&c, 1), [failed_ref, this](const error_code &e, size_t size) {
        if (*failed_ref) return;  // Ignore event if digio client is already destroyed or it failed
        syn_read_callback(e, size);
    });
}

template<typename Executor>
void client<Executor>::async_read_header()
{
    using namespace _impl;

    shared_ptr<atomic_bool> failed_ref = failed;
    async_read_until(serial, buff, "\r\n", [failed_ref, this](const error_code &e, size_t size) {
        if (*failed_ref) return;
        init_read_callback(e, size);
    });
}

template<typename Executor>
void client<Executor>::async_read_event()
{
    using namespace _impl;

    shared_ptr<atomic_bool> failed_ref = failed;
    async_read_until(serial, buff, "\r\n", [failed_ref, this](const error_code &e, size_t size) {
        if (*failed_ref) return;
        read_callback(e, size);
    });
}

template <typename Executor>
void client<Executor>::async_init()
{
    using namespace _impl;

    // cout << "Initializing..." << endl;

    try {
        syn = make_unique<syn_buffer>();
        // serial.send_break();
        // bind(&client<Executor>::syn_read_callback, this, _1, _2)
        async_read_syn();

        // TODO: Send also a syn word from client to digio host
    }
    catch (const std::exception &e) {
        handle_exception(e);
    }
}

template <typename Executor>
void client<Executor>::syn_read_callback(const boost::system::error_code &error, std::size_t bytes_transferred)
{
    using namespace _impl;

    (void) bytes_transferred;

    assert(!*failed);

    if (error) {
        handle_exception(system_error(error));
        return;
        // init_callback(error, vector<bool>(), vector<bool>());
    }

    try {
        *syn << c;
        if (*syn == _impl::syn_word) {
            syn.reset();
            async_read_header();
        }
        else {
            async_read_syn();
        }
    }
    catch (const std::exception &e) {
        handle_exception(e);
    }
}

// size is:
// "The number of bytes in the dynamic buffer sequence's
// get area up to and including the delimiter."
template<typename Executor>
void client<Executor>::init_read_callback(const boost::system::error_code &error, std::size_t size)
{
    using namespace _impl;

    assert(!*failed);

    if (error) {
        handle_exception(system_error(error));
        return;
    }

    try {
        string::const_iterator line_begin = cbegin(buff_string);
        string::const_iterator line_end = line_begin + size;

        vector<bool> out;

        json j = json::parse(line_begin, line_end);

        // Erase this line
        buff_string.erase(0, size);

        input_pins = j["in"].get<vector<int>>();

        output_pins = j["out"].get<vector<int>>();

        if (cb) cb->on_init();

        async_read_event();
    }
    catch (const std::exception &e) {
        handle_exception(e);
    }
}

/*
inline void escape_escape_sequences(std::string &str) {
  std::pair<char, char> const sequences[] {
    { '\a', 'a' },
    { '\b', 'b' },
    { '\f', 'f' },
    { '\n', 'n' },
    { '\r', 'r' },
    { '\t', 't' },
    { '\v', 'v' },
  };

  for (size_t i = 0; i < str.length(); ++i) {
    char *const c = str.data() + i;

    for (auto const &seq : sequences) {
      if (*c == seq.first) {
        *c = seq.second;
        str.insert(i, "\\");
        ++i; // to account for inserted "\\"
        break;
      }
    }
  }
}
*/

template<typename Executor>
void client<Executor>::read_callback(const boost::system::error_code &error, std::size_t size)
{
    using namespace _impl;

    assert(!*failed);

    if (error) {
        handle_exception(system_error(error));
        return;
    }

    try {
        // string copy (buff_string);
        // escape_escape_sequences(copy);

        // cout << "Buffer: " << copy << endl;

        string::const_iterator line_begin = cbegin(buff_string);
        string::const_iterator line_end = line_begin + size;

        // cerr << "Digio: Line received: " << quoted(string_view(line_begin, line_end)) << endl;

        /*
        cout << "Line: ";
        for (string::const_iterator it = line_begin; it != line_end; it++) {
            cout << *it;
        }
        cout << endl;
        */

        json j = json::parse(line_begin, line_end);
        buff_string.erase(0, size);
        json ack = j["ack"];
        if (!ack.empty()) {
            // TODO: Handle digital write acks
        }
        else {
            json in = j["in"];
            if (in.empty()) {
                cerr << "Digio: Warning: No \"in\" key!" << endl;
                /* for (auto iter = line_begin; iter != line_end; iter++) {
                    cerr << *iter;
                }
                cerr << endl; */
            }
            else {
                if (cb) {
                    if (in.is_array()) {
                        size_t i = 0;
                        json::iterator it = in.begin();
                        // input_pins_t::iterator input_pin_it = input_pins.begin();
                        for (; it != in.end(); ++it, /* ++input_pin_it, */ ++i) {
                            int val = it->get<int>();
                            input_pins.at(i) = val;
                            cb->on_input_change(i, val);
                        }
                    }
                    else if (in.is_object()) {
                        size_t i = 0;
                        json::iterator it = in.begin();
                        for (; it != in.end(); ++it, ++i) {
                            int pin = stoi(it.key());
                            bool value = it.value().get<int>();
                            input_pins.at(pin) = value;
                            cb->on_input_change(pin, value);
                        }
                    }
                    else {
                        throw digio::exception();
                    }
                }
            }
        }

        async_read_event();
    }
    catch (const std::exception &e) {
        handle_exception(e);
    }
}

template<typename Executor>
void client<Executor>::write_callback(const boost::system::error_code &error, std::size_t size, int pin, int value)
{
    using namespace _impl;

    (void) size;

    assert(!*failed);

    if (error) {
        handle_exception(system_error(error));
        return;
    }

    output_pins.at(pin) = value;

    if (cb) cb->on_output_change(pin, value);
}

template<typename Executor>
void client<Executor>::digital_write(int pin, bool value)
{
    using namespace _impl;

    if (*failed) return;

    try {
        json j = {{"out", {{to_string(pin), value}}}};
        ostringstream ss;
        ss << j << endl;
        shared_ptr<string> msg = make_shared<string>(ss.str());
        shared_ptr<atomic_bool> failed_ref = failed;
        async_write(serial, buffer(*msg), [failed_ref, this, msg, pin, value] (const error_code &error, size_t size) {
            (void) msg;
            if (*failed_ref) return;
            write_callback(error, size, pin, value);
        });
        // TODO: user operation success callback
    }
    catch (const std::exception &e) {
        handle_exception(e);
    }
}

template<typename Executor>
void client<Executor>::cancel() {
    *failed = true;
    serial.cancel();
}

template<typename Executor>
void client<Executor>::close() {
    *failed = true;
    serial.close();
}

}

#endif // CLIENT_IMPL_HPP
