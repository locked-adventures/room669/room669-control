#include <iostream>
#include "roomcontrol.hpp"
#include <boost/asio/io_context.hpp>
#include <boost/asio/serial_port.hpp>
#include <utility>
#include <boost/asio/steady_timer.hpp>

#include <ola/Logging.h>
#include <ola/DmxBuffer.h>

#include <nlohmann/json.hpp>
#include <fstream>
#include <map>
#include <vector>
#include <cstdint>
#include <Eigen/Core>
#include <portaudiocpp/AutoSystem.hxx>

#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))
#define IS_POSIX
#endif

#ifdef IS_POSIX
#include <termios.h>
#include <cstdio>
#endif

static const char CONFIG_FILE[] = "/etc/room669_control.conf";

static const char MQTT[] = "mqtt";
static const char BROKER[] = "broker";
static const char CLIENTS[] = "clients";
static const char ME[] = "me";
static const char GHOST_TV[] = "ghost_tv";
static const char MUSIC_BOX[] = "music_box";

static const char DIGIO[] = "digio";
static const char PORT[] = "port";
static const char BAUD_RATE[] = "baud_rate";

static const char OLA[] = "ola";
static const char HAZER[] = "hazer";
static const char CHANNEL[] = "channel";
static const char DURATION[] = "duration"; // in ms
static const char DMX_SCENES[] = "dmx_scenes";
static const char UNIVERSE[] = "universe";

static const char MULTIAUDIO[] = "multiaudio";
static const char SOUNDS[] = "sounds";

static const char RIDDLES[] = "riddles";

static const char LOG_FILE[] = "log_file";


namespace ola {
void from_json(const nlohmann::json& j, DmxBuffer& dmxBuffer) {
    typedef std::vector<uint8_t> vect;
    vect v = j.get<vect>();
    dmxBuffer.Set(v.data(), v.size());
}
}

using json = nlohmann::json;

namespace Eigen {
    template<typename Scalar, int Rows, int Cols>
    void to_json(json& j, const Matrix<Scalar, Rows, Cols>& m) {
        j = json::array();
        for (int row = 0; row < m.rows(); row++) {
            json row_json = json::array();
            for (int col = 0; col < m.cols(); col++) {
                row_json.push_back(m(row, col));
            }
            j.push_back(std::move(row_json));
        }
    }

    template<typename Scalar, int Rows, int Cols>
    void from_json(const json& j, Matrix<Scalar, Rows, Cols>& m) {
        int rows = j.size();
        int cols = 0;
        if (rows > 0) {
            cols = j.at(0).size();
        }
        m = Matrix<Scalar, Rows, Cols>(rows, cols);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                m(row, col) = j.at(row).at(col);
            }
        }
    }
}

namespace room669 {
    void to_json(json& j, const multisound_pref& p) {
        j = json{{"file", p.file}, {"map", p.map}};
    }

    void from_json(const json& j, multisound_pref& p) {
        j.at("file").get_to(p.file);
        j.at("map").get_to(p.map);
    }

    void to_json(json& j, const std::map<std::string, multisound_pref>& p) {
        j = json();
        for (const auto &[lang, sound_pref]: p) {
            j[lang] = sound_pref;
        }
    }

    void from_json(const json& j, std::map<std::string, multisound_pref>& p) {
        try {
            // One universal sound
            j.at("file").get_to(p[LANG_OTHER].file);
            j.at("map").get_to(p[LANG_OTHER].map);
        }
        catch (const json::out_of_range&) {
            // Different sounds for different languages
            for (json::const_iterator it = j.cbegin(); it != j.cend(); ++it) {
              p[it.key()] = it.value();
            }
        }
    }

    void to_json(json& j, const riddle_config& r) {
        j = json{{"input_pin", r.input_pin}, {"output_pin", r.output_pin}};
    }

    void from_json(const json& j, riddle_config& r) {
        j.at("input_pin").get_to(r.input_pin);
        j.at("output_pin").get_to(r.output_pin);
        r.output_active = 0; // Output is active LOW by default
        auto level = j.find("output_active");
        if (level != j.end()) {
            level->get_to(r.output_active);
        }
    }
}

int main()
{
    using std::cout;
    using std::cerr;
    using std::endl;
    using std::map;
    typedef std::string str;
    using std::ifstream;
    using room669::RoomControl;
    using room669::multisound_pref;
    using room669::riddle_config;
    using nlohmann::json;
    using ola::DmxBuffer;
    using boost::asio::io_context;
    using boost::asio::serial_port;
    typedef unsigned uns;

    cout << "Program start." << endl;

    portaudio::AutoSystem portAudio;

    cout << "Reading and parsing config file..." << endl;
    ifstream cfg_stream;
    cfg_stream.exceptions(ifstream::failbit | ifstream::badbit);
    cfg_stream.open(CONFIG_FILE);
    json config = json::parse(cfg_stream);
    cout << "Done." << endl;

    // Initialize OLA logging
    ola::InitLogging(ola::OLA_LOG_WARN, ola::OLA_LOG_STDERR);

    io_context ioc;

    serial_port serial(ioc, config[DIGIO][PORT].get<str>());
    serial.set_option(serial_port::baud_rate(config[DIGIO][BAUD_RATE].get<uns>()));
    serial.set_option(serial_port::flow_control(serial_port::flow_control::none));
    serial.set_option(serial_port::parity(serial_port::parity::none));
    serial.set_option(serial_port::stop_bits(serial_port::stop_bits::one));
    serial.set_option(serial_port::character_size(8));

#ifdef IS_POSIX
    // Set POSIX specific serial configuration
    using std::perror;
    int fd = serial.native_handle();
    struct termios tio;
    if (tcgetattr(fd, &tio) == -1) {
        perror("tcgetattr");
    }
    tio.c_cc[VTIME] = 0;
    tio.c_cc[VMIN] = 1;
    if (tcflush(fd, TCIFLUSH) == -1) {
        perror("tcflush");
    }
    if (tcsetattr(fd, TCSANOW, &tio) == -1) {
        perror("tcsetattr");
    }
#endif

    typedef map<str, DmxBuffer> buffer_map;
    buffer_map dmx_scenes = config[OLA][DMX_SCENES].get<buffer_map>();

    typedef map<
            str /* sound name */,
            map<str /* lang */, multisound_pref>
    > sound_map;
    sound_map sounds = config[MULTIAUDIO][SOUNDS].get<sound_map>();

    typedef map<str, riddle_config> riddle_map;
    riddle_map riddle_configs = config[RIDDLES].get<riddle_map>();

    RoomControl roomControl(
        config[MQTT][BROKER].get<str>(),
        config[MQTT][CLIENTS][ME].get<str>(),
        config[MQTT][CLIENTS][GHOST_TV].get<str>(),
        config[MQTT][CLIENTS][MUSIC_BOX].get<str>(),
        ioc.get_executor(),
        std::move(serial),
        config[OLA][UNIVERSE].get<uns>(),
        dmx_scenes,
        config[OLA][HAZER][CHANNEL].get<uns>(),
        config[OLA][HAZER][DURATION].get<uns>(),
        sounds,
        riddle_configs,
        config.value(LOG_FILE, "")
    );

    roomControl.async_init();
    ioc.run();

    return 0;
}
