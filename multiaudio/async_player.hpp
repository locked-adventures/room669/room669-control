#ifndef MULTIAUDIO_PLAYER_HPP
#define MULTIAUDIO_PLAYER_HPP

#include <sndfile.hh>
#include <thread>
#include <string>
#include <stop_token>
#include <stdexcept>
#include <utility>
#include <portaudiocpp/Device.hxx>
#include <portaudiocpp/System.hxx>
#include <Eigen/Core>
#include <atomic>
#include <functional>
#include <mutex>
#include <memory>

namespace multiaudio {

enum {
    SUCCESS,
    NOFILE,
    ABORTED,
    EXCEPTION
};

class control_block {
    mutable std::mutex m;
    bool repeat;
    std::function<void(int)> completion_callback;

public:
    control_block(bool repeat = false, std::function<void(int)> completion_callback = nullptr)
        : repeat(repeat)
        , completion_callback(completion_callback)
    {}

    bool get_repeat() const {
        std::lock_guard<std::mutex> l(m);
        return repeat;
    }

    void set_repeat(bool value) {
        std::lock_guard<std::mutex> l(m);
        repeat = value;
    }

    std::function<void (int)> get_completion_callback() const {
        std::lock_guard<std::mutex> l(m);
        return completion_callback;
    }

    void set_completion_callback(const std::function<void (int)> &value) {
        std::lock_guard<std::mutex> l(m);
        completion_callback = value;
    }
};

class async_player
{
    std::shared_ptr<control_block> control;

    class thread_functor_t {
        SndfileHandle sndfile_handle;
        Eigen::MatrixXf channel_mapping;
        portaudio::Device *playback_device;
        unsigned long frames_per_buffer;
        std::shared_ptr<control_block> control;
        bool dummy;

    public:
        thread_functor_t() {}

        thread_functor_t (
                std::string sound_file,
                Eigen::MatrixXf channel_mapping,
                portaudio::Device &playback_device = portaudio::System::instance().defaultOutputDevice(),
                unsigned long frames_per_buffer = 512,
                std::shared_ptr<control_block> control = nullptr
                )
            : sndfile_handle()
            , channel_mapping(std::move(channel_mapping))
            , playback_device(&playback_device)
            , frames_per_buffer(frames_per_buffer)
            , control(control)
            , dummy(true)
        {
            if (sound_file.size()) {
                sndfile_handle = SndfileHandle(sound_file);
                if (sndfile_handle.error()) {
                    throw std::runtime_error("Failed to open sound file");
                }
                if (sndfile_handle.channels() < this->channel_mapping.cols()) {
                    throw std::runtime_error("The sound file does not have enough channels");
                }
                dummy = false;
            }
        }

        void operator ()(std::stop_token &&stop);
    };

    thread_functor_t thread_functor;
    std::jthread playing_thread;

public:
    async_player() {}

    async_player (
            std::string sound_file,
            Eigen::MatrixXf channel_mapping,
            bool repeat = false,
            portaudio::Device &playback_device = portaudio::System::instance().defaultOutputDevice(),
            unsigned long frames_per_buffer = 512,
            std::function<void(int)> completion_callback = nullptr
    )
        : control(std::make_shared<control_block>(repeat, completion_callback))
        , thread_functor(
              std::move(sound_file),
              std::move(channel_mapping),
              playback_device,
              frames_per_buffer,
              control
              )
        , playing_thread()
    {}

    async_player &set_callback(std::function<void(int)> completion_callback) {
        control->set_completion_callback(completion_callback);
        return *this;
    }

    async_player &start() {
        playing_thread = std::jthread(thread_functor);
        return *this;
    }

    async_player &stop() {
        playing_thread.request_stop();
        return *this;
    }

    async_player &set_repeat(bool repeat) {
        control->set_repeat(repeat);
        return *this;
    }

    async_player &join() {
        playing_thread.join();
        return *this;
    }
};

}

#endif // MULTIAUDIO_PLAYER_HPP
