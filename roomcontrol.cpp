#include "roomcontrol.hpp"

#include <mqtt/connect_options.h>

#include <iostream>

#include <stdexcept>
#include <chrono>
#include <cassert>
#include <sstream>
#include <iomanip>
#include <cctype>

namespace room669
{

namespace _impl {
using std::to_string;

using std::chrono::system_clock;
typedef std::chrono::milliseconds millis;

using std::tolower;

using std::cout;
using std::cerr;
using std::endl;
using std::runtime_error;

typedef std::recursive_mutex mutex_t;

using std::lock_guard;

using std::chrono::seconds;
using std::chrono::milliseconds;

using boost::system::error_code;

using std::string;
using std::exception;
using std::vector;
using std::stoi;

using std::shared_ptr;
using std::unique_ptr;
using std::make_shared;
using std::make_unique;

using std::ostringstream;

using std::setfill;
using std::setw;

namespace rdl = riddle_names;
namespace snd = sound_names;
namespace dmx = dmx_scene_names;

using std::out_of_range;

using std::atomic_long;
}

const ola::client::StreamingClientInterface::SendArgs dmx_send_args;

void RoomControl::on_failure(const mqtt::token &asyncActionToken)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    (void) asyncActionToken;
    cout << "Mqtt: on_failure" << endl;
}

void RoomControl::on_success(const mqtt::token &asyncActionToken)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    (void) asyncActionToken;
    cout << "Mqtt: on_success" << endl;
}

void RoomControl::connected(const mqtt::string &cause)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: connected";
    if (cause.size()) {
        cout << ": " << cause;
    }
    cout << endl;

    try {
        mqtt_client->subscribe(mqtt_client->get_client_id() + "/set/#", 0);
        mqtt_client->subscribe(mqtt_ghost_tv_id + "/get/#", 0);

        if (digio_client.input_pins.size()) {
            size_t i = 0;
            for (vector<int>::const_iterator iter = digio_client.input_pins.cbegin(); iter != digio_client.input_pins.cend(); iter++, i++) {
                publish_input_value(i, *iter);
            }
        }
        else {
            // Input pin values are not yet known
            for (size_t i = 0; i < N_INPUT_PINS; i++) {
                publish_input_value(i, -1);
            }
        }

        if (digio_client.output_pins.size()) {
            size_t i = 0;
            for (vector<int>::const_iterator iter = digio_client.output_pins.cbegin(); iter != digio_client.output_pins.cend(); iter++, i++) {
                publish_output_value(i, *iter);
            }
        }
        else {
            // Output pin values are not yet known
            for (size_t i = 0; i < N_OUTPUT_PINS; i++) {
                publish_output_value(i, -1);
            }
        }

        string clientId = mqtt_client->get_client_id();

        publish_all_riddle_states();
        mqtt_client->publish(mqtt_ghost_tv_id + "/set/lang", lang);
        mqtt_client->publish(clientId + "/get/num_of_group_members", to_string(n_group_members), 0, true);
        mqtt_client->publish(clientId + "/get/lang", lang, 0, true);
        mqtt_client->publish(clientId + "/get/mean_member_age", to_string(mean_member_age), 0, true);
        mqtt_client->publish(clientId + "/get/previous_experience", to_string(previous_experience), 0, true);
        mqtt_client->publish(clientId + "/status", "ONLINE", 0, true);
        cout << "Mqtt: Status and riddle states published" << endl;
    }
    catch (mqtt::exception &e) {
        cout << "Mqtt: Cannot publish information: " << e.what() << endl;
    }
}

void RoomControl::connection_lost(const mqtt::string &cause)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: connection lost";
    if (cause.size()) {
        cout << ": " << cause;
    }
    cout << endl;
}

void RoomControl::message_arrived(mqtt::const_message_ptr msg_ptr)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: message arrived on topic: " << msg_ptr->get_topic() << endl;

    string topic = msg_ptr->get_topic();

    string client_id = mqtt_client->get_client_id();
    string method_set_riddle = client_id + "/set/riddle/";
    string method_set_output = client_id + "/set/output/";
    string method_set_sound = client_id + "/set/sound/";
    string method_set_light_scene = client_id + "/set/lightscene";
    string method_set_lang = client_id + "/set/lang";
    string method_set_n_group_members = client_id + "/set/num_of_group_members";
    string method_set_mean_member_age = client_id + "/set/mean_member_age";
    string method_set_previous_experience = client_id + "/set/previous_experience";

    if (topic.starts_with(method_set_riddle)) {
        string riddle = topic.substr(method_set_riddle.size());

        if (riddle == "all") { // Special case to solve or reset all riddles
            if (msg_ptr->get_payload().starts_with("0")) {
                reset_all();
            }
            else {
                solve_all();
            }
        }
        else {
            if (msg_ptr->get_payload().starts_with("0")) {
                reset(riddle);
            }
            else {
                solve(riddle, true);
            }
        }
    }
    else if (topic.starts_with(method_set_output)) {
        int pin = -1;
        try {
            pin = stoi(topic.substr(method_set_output.size()));
        }
        catch (const exception &e) {
            cout << "Invalid output pin identifier" << endl;
        }

        if (pin >= 0) {
            digio_client.digital_write(pin, !msg_ptr->get_payload().starts_with("0"));
        }
    }
    else if (topic.starts_with(method_set_sound)) {
        string sound = topic.substr(method_set_sound.size());
        if (msg_ptr->get_payload().starts_with("0")) {
            cout << "Stop sound " << sound << "..." << endl;
            try {
                stop_sound(sound.data());
                cout << "Done." << endl;
            }
            catch (const out_of_range &oor) {
                cout << "No such sound!" << endl;
            }
        }
        else {
            cout << "Start sound " << sound << "..." << endl;
            try {
                start_sound(sound.data());
                cout << "Done." << endl;
            }
            catch (const out_of_range &oor) {
                cout << "No such sound!" << endl;
            }
        }
    }
    else if (topic == method_set_light_scene) {
        try {
            dmx_buffer = dmx_scenes.at(msg_ptr->get_payload());
            if (!ola_client.SendDmx(ola_universe, dmx_buffer)) {
                std::cout << "Cannot send DMX scene" << std::endl;
            }
        }
        catch (const out_of_range &oor) {
            cout << "No such lightscene" << endl;
        }
    }
    else if (topic == mqtt_ghost_tv_id + "/get/solved") {
        if (!msg_ptr->get_payload().starts_with("0")) {
            tv_timer.expires_after(seconds(15));
            tv_timer.async_wait([this](const boost::system::error_code &e) {
                if (!e)
                    solve(rdl::TV);
            });
        }
    }
    else if (topic == method_set_lang) {
        string payload = msg_ptr->get_payload();
        size_t size = payload.size();
        if (size < 3) {
            strncpy(lang, payload.data(), size);
            lang[size] = '\0';
            for (char *iter = lang; *iter; ++iter) {
                *iter = tolower(*iter);
            }
            cout << "Language: " << lang << endl;
            try {
                mqtt_client->publish(mqtt_ghost_tv_id + "/set/lang", lang);
                mqtt_client->publish(mqtt_client->get_client_id() + "/get/lang", lang, 0, true);
            }
            catch (const mqtt::exception &e) {
                cout << "Mqtt: publish lang exception: " << e.what() << endl;
            }
        }
        else {
            cout << "Language id is too long!" << endl;
        }
    }
    else if (topic == method_set_n_group_members) {
        string payload = msg_ptr->get_payload();
        try {
            n_group_members = stoi(msg_ptr->get_payload());
            try {
                mqtt_client->publish(mqtt_client->get_client_id() + "/get/num_of_group_members", to_string(n_group_members), 0, true);
            }
            catch (const mqtt::exception &e) {
                cout << "Mqtt: cannot publish number of members: " << e.what() << endl;
            }
        }
        catch (const std::invalid_argument &e) {
            cout << "Cannot parse number of members: " << e.what() << endl;
        }
        catch (const std::out_of_range &e) {
            cout << "Number of members out of range: " << e.what() << endl;
        }
    }
    else if (topic == method_set_mean_member_age) {
        string payload = msg_ptr->get_payload();
        try {
            mean_member_age = stoi(msg_ptr->get_payload());
             try {
                mqtt_client->publish(mqtt_client->get_client_id() + "/get/mean_member_age", to_string(mean_member_age), 0, true);
            }
            catch (const mqtt::exception &e) {
                cout << "Mqtt: cannot publish mean member age: " << e.what() << endl;
            }
        }
        catch (const std::invalid_argument &e) {
            cout << "Cannot parse mean member age: " << e.what() << endl;
        }
        catch (const std::out_of_range &e) {
            cout << "Mean member age out of range: " << e.what() << endl;
        }
    }
    else if (topic == method_set_previous_experience) {
        string payload = msg_ptr->get_payload();
        try {
            previous_experience = stoi(msg_ptr->get_payload());
            try {
                mqtt_client->publish(mqtt_client->get_client_id() + "/get/previous_experience", to_string(previous_experience), 0, true);
            }
            catch (const mqtt::exception &e) {
                cout << "Mqtt: cannot publish mean member age: " << e.what() << endl;
            }
        }
        catch (const std::invalid_argument &e) {
            cout << "Cannot parse mean member age: " << e.what() << endl;
        }
        catch (const std::out_of_range &e) {
            cout << "Mean member age out of range: " << e.what() << endl;
        }
    }
}

void RoomControl::delivery_complete(mqtt::delivery_token_ptr)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Mqtt: message delivery completed" << endl;
}

void RoomControl::on_error(const std::exception &e)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Digio: Exception: " << e.what() << endl;
}

void RoomControl::on_init()
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Digio: Initialized" << endl;

    cout << "[";
    bool next = false;
    for (int val: digio_client.input_pins) {
        if (next) {
            cout << ", ";
        }
        cout << val;
        next = true;
    }
    cout << "]" << endl;

    try {
        size_t i = 0;
        for (vector<int>::const_iterator iter = digio_client.input_pins.cbegin(); iter != digio_client.input_pins.cend(); iter++, i++) {
            publish_input_value(i, *iter);
        }
        i = 0;
        for (vector<int>::const_iterator iter = digio_client.output_pins.cbegin(); iter != digio_client.output_pins.cend(); iter++, i++) {
            publish_output_value(i, *iter);
        }
    }
    catch (mqtt::exception &e) {
        cout << "Mqtt: Cannot publish initial pin values: " << e.what() << endl;
    }

    cout << "Settings all output pins to the default value..." << endl;
    for (auto &[name, riddle] : riddles) {
        riddle->reset();
    }
    cout << "Done." << endl;
}

void RoomControl::on_input_change(int pin, int value)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Digio: Pin " << pin << " is " << (value ? "HIGH" : "LOW") << endl;

    try {
        publish_input_value(pin, value);
    }
    catch (mqtt::exception &e) {
        cout << "Mqtt: Cannot publish input pin value: " << e.what() << endl;
    }

    if (value == INPUT_ACTIVE) {
        if (pin >= 0 && unsigned(pin) < N_INPUT_PINS) {
            const std::string &riddle = input_pin_to_riddle[pin];
            if (riddle.size()) {
                solve(input_pin_to_riddle[pin]);
            }
        }
    }

    /* if (pin == 0 && !value) {
        cout << "OLA: Setting first channel to 255..." << endl;
        dmx_buffer.SetChannel(0, 255);
        if (!ola_client.SendDMX(ola_universe, dmx_buffer, dmx_send_args)) {
            cout << "OLA: Send DMX failed!" << endl;
        }
        else {
            cout << "OLA: DMX sent." << endl;
        }
    }*/
}

void RoomControl::on_output_change(int pin, int value)
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    if (exiting) return;

    cout << "Digio: Output " << pin << " is " << (value ? "HIGH" : "LOW") << endl;

    try {
        publish_output_value(pin, value);
    }
    catch (mqtt::exception &e) {
        cout << "Mqtt: Cannot publish output pin value: " << e.what() << endl;
    }
}

void RoomControl::solve(const std::string &riddle, bool admin_solve)
{
    using namespace _impl;

    iriddle *r;

    try {
        r = riddles.at(riddle).get();
    }
    catch (const std::out_of_range &e) {
        cout << "No such riddle" << endl;
        return;
    }

    if (r->is_solved()) {
        cout << "Riddle " << riddle << " is already solved" << endl;
    }
    else {
        r->solve(admin_solve);
        cout << "Riddle " << riddle << " solved" << endl;
        try  {
            mqtt_client->publish(mqtt_client->get_client_id() + "/get/riddle/" + riddle, "1", 0, true);
        }
        catch (const exception &e) {
            cout << "Cannot publish riddle state" << endl;
        }
    }
}

void RoomControl::solve_all()
{
    for (const decltype(riddles)::value_type &pair : riddles) {
        solve(pair.first, true);
    }
}

void RoomControl::reset(const std::string &riddle)
{
    using namespace _impl;

    iriddle *r;

    try {
        r = riddles.at(riddle).get();
    }
    catch (const std::out_of_range &e) {
        cout << "No such riddle" << endl;
        return;
    }

    if (r->is_solved()) {
        r->reset();
        cout << "Riddle " << riddle << " reset" << endl;
        try  {
            mqtt_client->publish(mqtt_client->get_client_id() + "/get/riddle/" + riddle, "0", 0, true);
        }
        catch (const exception &e) {
            cout << "Cannot publish riddle state" << endl;
        }
    }
    else {
        cout << "Riddle " << riddle << " is already reset" << endl;
    }
}

void RoomControl::reset_all()
{
    for (const decltype(riddles)::value_type &pair : riddles) {
        reset(pair.first);
    }
    using namespace dmx_scene_names;
    ola_client.SendDmx(ola_universe, dmx_scenes.at(RESETLIGHT));
    tv_timer.cancel();
}

void RoomControl::publish_all_riddle_states()
{
    for (const decltype(riddles)::value_type &pair : riddles) {
        mqtt_client->publish(mqtt_client->get_client_id() + "/get/riddle/" + pair.first,
                             pair.second->is_solved() ? "1" : "0",
                             0, true);
    }
}

void RoomControl::publish_input_value(int pin, int value)
{
    using namespace _impl;

    ostringstream topic;
    topic << mqtt_client->get_client_id() << "/get/input/" << setfill('0') << setw(2) << pin;
    ostringstream payload;
    payload << value;
    mqtt_client->publish(topic.str(), payload.str(), 0, true);
}

void RoomControl::publish_output_value(int pin, int value)
{
    using namespace _impl;

    ostringstream topic;
    topic << mqtt_client->get_client_id() << "/get/output/" << setfill('0') << setw(2) << pin;
    ostringstream payload;
    payload << value;
    mqtt_client->publish(topic.str(), payload.str(), 0, true);
}

RoomControl::~RoomControl()
{
    using namespace _impl;

    {
        lock_guard<mutex_t> l(m);

        cout << "~RoomControl() called" << endl;

        exiting = true;

        // Stop all sounds
        for (auto &[name, lang_players]: sounds) {
            for (auto &[lang, player]: lang_players) {
                player.stop();
            }
        }

        reset_all();
        digio_client.close();
    }

    try {
        mqtt_client->publish(mqtt_client->get_client_id() + "/status", "OFFLINE", 0, true)->wait();
        mqtt_client.reset();
    }
    catch (const exception &e) {
        cerr << "Mqtt: Client throwed exception on destruction" << endl;
    }
}

void RoomControl::async_init()
{
    using namespace _impl;

    lock_guard<mutex_t> l(m);

    cout << "RoomControl::async_init() entered" << endl;

    cout << "Connecting to OLA..." << endl;
    if (!ola_client.Setup()) {
        throw runtime_error("Cannot setup ola");
    }
    namespace dmx = dmx_scene_names;
    dmx_buffer = dmx_scenes.at(dmx::RESETLIGHT);
    if (!ola_client.SendDMX(ola_universe, dmx_buffer, dmx_send_args)) {
        cout << "OLA: Send DMX failed!" << endl;
    }
    cout << "Done." << endl;

    cout << "Initializing asynchronous DIGIO connection..." << endl;
    digio_client.async_init();
    cout << "Done." << endl;

    cout << "Initializing asynchronous MQTT connection..." << endl;
    mqtt::connect_options options = mqtt::connect_options_builder()
            .automatic_reconnect(true)
            .will(mqtt::message(mqtt_client->get_client_id() + "/status", "FAILED", 0, true))
            .finalize();
    mqtt_client->connect(options, nullptr, *this);
    cout << "Done." << endl;

    cout << "RoomControl::async_init() returns" << endl;
}

void RoomControl::riddle_pregame::solve(bool by_admin)
{
    using namespace _impl;

    set_dmx_scene(rdl::PREGAME);
    digital_write(output_pin, output_active);
    set_solved_timestamp(date::floor<millis>(system_clock::now()));
    solved = true;
    admin_solved = by_admin;
}

void RoomControl::riddle_pregame::reset()
{
    using namespace _impl;

    digital_write(output_pin, !output_active);
    solved = false;
    set_solved_timestamp(std::nullopt);
}

void RoomControl::riddle_gamestart::solve(bool by_admin)
{
    using namespace _impl;

    set_dmx_scene(dmx::STARTGAME);

    shared_ptr<atomic_long> gen_ref = generation;
    long gen = *gen_ref;
    start_sound(snd::STARTGAME, [gen_ref, gen, this] (int err) {
        if (err != multiaudio::SUCCESS) return;
        if (*gen_ref != gen) return;
        auto l = create_lock();

        timer.expires_after(seconds(2));
        timer.async_wait([gen_ref, gen, this] (const error_code &err) {
            if (err) return;
            if (*gen_ref != gen) return;
            auto l = create_lock();

            digital_write(output_pin, output_active);
            set_dmx_scene(dmx::EMERGENCY);

            start_sound(snd::SHORT_CIRCUIT, [gen_ref, gen, this] (int err) {
                if (err != multiaudio::SUCCESS) return;
                if (*gen_ref != gen) return;
                auto l = create_lock();

                timer.expires_after(seconds(25));
                timer.async_wait([gen_ref, gen, this] (const error_code &err) {
                    if (err) return;
                    if (*gen_ref != gen) return;
                    auto l = create_lock();

                    start_sound(snd::ATMO1);
                });
            });
        });
    });

    solved = true;
    admin_solved = by_admin;
    set_solved_timestamp(date::floor<millis>(system_clock::now()));
}

void RoomControl::riddle_gamestart::reset()
{
    using namespace _impl;

    digital_write(output_pin, !output_active);

    stop_sound(snd::STARTGAME);
    stop_sound(snd::SHORT_CIRCUIT);
    stop_sound(snd::ATMO1);

    solved = false;
    admin_solved = false;
    set_solved_timestamp(std::nullopt);

    (*generation)++;
}

void RoomControl::riddle_bible::solve(bool by_admin)
{
    using namespace _impl;

    digital_write(output_pin, output_active);
    set_dmx_scene(dmx::EMERGENCY);
    solved = true;
    admin_solved = by_admin;
    set_solved_timestamp(date::floor<millis>(system_clock::now()));

    shared_ptr<atomic_long> gen_ref = generation;
    long gen = *gen_ref;

    stop_sound(snd::ATMO1);

    try {
        control->mqtt_client->publish(control->mqtt_ghost_tv_id + "/set/power", "0");
    }
    catch (const mqtt::exception &e) {
        cout << "Mqtt: cannot send command to turn off ghost tv: " << e.what() << endl;
    }


    timer.expires_after(seconds(3));
    timer.async_wait([gen_ref, gen, this] (const error_code &err) {
        if (err) return;
        auto l = create_lock();
        if (*gen_ref != gen) return;

        control->dmx_buffer = control->dmx_scenes.at(dmx::BIBLE);
        control->dyn_candle_light.start();
    });

    start_sound(snd::BIBLE, [gen_ref, gen, this] (int err) {
        if (err != multiaudio::SUCCESS) return;
        if (*gen_ref != gen) return;
        auto l = create_lock();

        control->dyn_candle_light.stop();
        control->dmx_buffer = control->dmx_scenes.at(dmx::BIBLE);
        control->update_dmx();
        timer2.expires_after(seconds(3));
        timer2.async_wait([gen_ref, gen, this] (const error_code &err) {
            if (err) return;
            if (*gen_ref != gen) return;
            auto l = create_lock();

            start_sound(snd::ATMO2);
        });
    });
}

void RoomControl::riddle_bible::reset()
{
    using namespace _impl;

    digital_write(output_pin, !output_active);
    stop_sound(snd::BIBLE);
    stop_sound(snd::ATMO2);
    control->dyn_candle_light.stop();
    solved = false;
    admin_solved = false;
    set_solved_timestamp(std::nullopt);

    (*generation)++;
}

void RoomControl::riddle_time_travel::solve(bool by_admin)
{
    using namespace _impl;

    digital_write(output_pin, output_active);
    solved = true;
    admin_solved = by_admin;
    set_solved_timestamp(date::floor<millis>(system_clock::now()));

    set_dmx_scene(dmx::TIME_TRAVEL);
    stop_sound(snd::ATMO2);
    start_sound(snd::TIME_TRAVEL);

    cout << "Hazer on" << endl;
    hazer_on();

    shared_ptr<atomic_long> gen_ref = generation;
    long gen = *gen_ref;

    hazer_timer.expires_after(milliseconds(get_hazer_duration()));
    hazer_timer.async_wait([gen_ref, gen, this] (const error_code &err) {
        if (err || *gen_ref != gen) return;
        auto l = create_lock();

        cout << "Hazer off" << endl;
        hazer_off();
    });

    timer.expires_after(seconds(15));
    timer.async_wait([gen_ref, gen, this] (const error_code &err) {
        if (err || *gen_ref != gen) return;
        auto l = create_lock();

        start_sound(snd::ATMO3);

        timer.expires_after(seconds(15));
        timer.async_wait([gen_ref, gen, this] (const error_code &err) {
            if (err) return;
            if (*gen_ref != gen) return;
            auto l = create_lock();

            start_sound(snd::ATMO4);
        });
    });
}

void RoomControl::riddle_time_travel::reset()
{
    using namespace _impl;

    (*generation)++;
    timer.cancel();
    hazer_timer.cancel();
    digital_write(output_pin, !output_active);
    stop_sound(snd::TIME_TRAVEL);
    stop_sound(snd::ATMO3);
    stop_sound(snd::ATMO4);
    hazer_off();
    solved = false;
    admin_solved = false;
    set_solved_timestamp(std::nullopt);
}

void RoomControl::riddle_cabinet::solve(bool by_admin)
{
    using namespace _impl;

    set_dmx_scene(dmx::EMERGENCY);
    start_sound(snd::CABINET);

    long gen = *generation;
    shared_ptr<atomic_long> gen_ref = generation;

    timer.expires_after(std::chrono::seconds(2));
    timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
        if (e || gen != *gen_ref)
            return;
        auto l = create_lock();

        stop_sound(snd::ATMO3);
        set_dmx_scene(dmx::CABINET);
        digital_write(output_pin, output_active);
    });

    solved = true;
    admin_solved = by_admin;
    set_solved_timestamp(date::floor<millis>(system_clock::now()));
}

void RoomControl::riddle_cabinet::reset()
{
    using namespace _impl;

    stop_sound(snd::CABINET);
    digital_write(output_pin, !output_active);
    timer.cancel();
    solved = false;
    admin_solved = false;
    set_solved_timestamp(std::nullopt);
    (*generation)++;
}

}
