#ifndef ROOMCONTROL_HPP
#define ROOMCONTROL_HPP

#include <mutex>
#include <mqtt/async_client.h>
#include <string>
#include <boost/asio/io_context.hpp>
#include "digio/client.hpp"
#include <boost/asio/serial_port.hpp>
#include <utility>
#include <boost/bind/bind.hpp>
#include <ola/DmxBuffer.h>
#include <ola/Logging.h>
#include <ola/client/StreamingClient.h>
#include <iterator>
#include <type_traits>
#include <utility>
#include <Eigen/Core>
#include "multiaudio/async_player.hpp"
#include <boost/asio/steady_timer.hpp>
#include <sstream>
#include <iomanip>
#include <atomic>
#include <cmath>
#include <chrono>
#include <optional>
#include "date.h"
#include <fstream>
#include <filesystem>

extern template class digio::client<>;

namespace room669
{
using namespace boost::placeholders;

extern const ola::client::StreamingClientInterface::SendArgs dmx_send_args;

constexpr int OUTPUT_ACTIVE = 0; // LOW

constexpr int INPUT_ACTIVE = 0; // LOW

constexpr unsigned N_INPUT_PINS = 16;

constexpr unsigned N_OUTPUT_PINS = 16;

namespace dmx_scene_names
{
constexpr char RESETLIGHT[] = "resetlight";
constexpr char PREGAME[] = "pregame";
constexpr char STARTGAME[] = "startgame";
constexpr char EMERGENCY[] = "emergency";

constexpr char FOUR_SWITCHES[] = "four_switches";
constexpr char DREHTEILE[] = "drehteile";
constexpr char TV[] = "tv";
constexpr char BIBLE[] = "bible";
constexpr char CANDLES[] = "candles";
constexpr char BOOKS[] = "books";
constexpr char TIME_TRAVEL[] = "time_travel";
constexpr char CABINET[] = "cabinet";
constexpr char DOLLS[] = "dolls";
constexpr char MUSIC_BOX[] = "music_box";
constexpr char GAMEWIN[] = "gamewin";
}

namespace sound_names
{
constexpr char STARTGAME[] = "startgame";
constexpr char SHORT_CIRCUIT[] = "short_circuit";
constexpr char ATMO1[] = "atmo1";
constexpr char ATMO2[] = "atmo2";
constexpr char ATMO3[] = "atmo3";
constexpr char ATMO4[] = "atmo4";

constexpr char FOUR_SWITCHES[] = "four_switches";
constexpr char DREHTEILE[] = "drehteile";
constexpr char TV[] = "tv";
constexpr char BIBLE[] = "bible";
constexpr char CANDLES[] = "candles";
constexpr char BOOKS[] = "books";
constexpr char TIME_TRAVEL[] = "time_travel";
constexpr char CABINET[] = "cabinet";
constexpr char DOLLS[] = "dolls";
constexpr char HATCH[] = "hatch";
constexpr char GAMEWIN[] = "gamewin";
}

namespace riddle_names
{
constexpr char PREGAME[] = "pregame";
constexpr char STARTGAME[] = "startgame";

constexpr char FOUR_SWITCHES[] = "four_switches";
constexpr char DREHTEILE[] = "drehteile";
constexpr char TV[] = "tv";
constexpr char BIBLE[] = "bible";
constexpr char CANDLES[] = "candles";
constexpr char BOOKS[] = "books";
constexpr char TIME_TRAVEL[] = "time_travel";
constexpr char CABINET[] = "cabinet";
constexpr char DOLLS[] = "dolls";
constexpr char MUSIC_BOX[] = "music_box";
constexpr char GAMEWIN[] = "gamewin";

constexpr const char *list[] = {
    PREGAME,
    STARTGAME,
    FOUR_SWITCHES,
    DREHTEILE,
    TV,
    BIBLE,
    CANDLES,
    BOOKS,
    TIME_TRAVEL,
    CABINET,
    DOLLS,
    MUSIC_BOX,
    GAMEWIN,
    nullptr
};
}

constexpr char TIME_FORMAT[] = "%FT%T";

constexpr char LANG_DEFAULT[] = "de";
constexpr char LANG_OTHER[] = "other";

struct multisound_pref {
    std::string file;
    Eigen::MatrixXf map;
};

struct riddle_config {
    int input_pin;
    int output_pin;
    int output_active;
};

class RoomControl;

class RoomControl : private mqtt::callback, private mqtt::iaction_listener, private digio::callback
{
    /**
     * @brief Riddle interface
     */
    struct iriddle {
        typedef std::optional<
            std::chrono::time_point<
                std::chrono::system_clock,
                std::chrono::milliseconds
            >
        >
        timestamp_t;

        virtual timestamp_t get_solved_timestamp() const = 0;

        virtual void set_solved_timestamp(const timestamp_t &timestamp) = 0;

        virtual bool get_admin_solved() const = 0;

        virtual void set_admin_solved(bool admin_solved) = 0;

        virtual int get_output_pin() const = 0;

        virtual void set_output_pin(int pin) = 0;

        virtual void set_output_active(int logic_level) = 0;

        /**
         * @brief set_solved - solve or reset riddle
         * @param solved - true to solve, false to reset
         */
        // virtual void set_solved(bool solve) = 0;

        virtual bool is_solved() const = 0;

        virtual void solve(bool by_admin = false) = 0;

        virtual void reset() = 0;

        virtual ~iriddle()
        {}
    };

    void start_sound(const char *sound, std::function<void(int)> callback = 0) {
        std::map<std::string /* lang */, multiaudio::async_player> &lang_players = sounds.at(sound);
        try {
            lang_players.at(lang).set_callback(callback).start();
        }
        catch (const std::out_of_range&) {
            lang_players.at(LANG_OTHER).set_callback(callback).start();
        }
    }

    void stop_sound(const char *sound) {
        std::map<std::string /* lang */, multiaudio::async_player> &lang_players = sounds.at(sound);
        for (auto &[lang, player]: lang_players) {
            player.stop();
        }
    }

    class basic_riddle : public iriddle {
    protected:
        RoomControl *control;
        bool solved;
        int output_pin;
        int output_active;
        timestamp_t solved_time;
        bool admin_solved;

        void update_dmx() {
            if (!control->ola_client.SendDmx(control->ola_universe, control->dmx_buffer)) {
                std::cout << "Cannot send DMX scene" << std::endl;
            }
        }

        void start_sound(const char *sound, std::function<void(int)> callback = 0) {
            control->start_sound(sound, callback);
        }

        void stop_sound(const char *sound) {
            control->stop_sound(sound);
        }

        void activate_ghost_tv() {
            try {
                control->mqtt_client->publish(control->mqtt_ghost_tv_id + "/set/power", "1");
            }
            catch (const mqtt::exception &e) {
                std::cout << "Mqtt exception: " << e.what() << std::endl;
            }
        }

        void activate_music_box() {
            try {
                control->mqtt_client->publish(control->mqtt_music_box_id + "/set/activate", "1");
            }
            catch (const mqtt::exception &e) {
                std::cout << "Mqtt exception: " << e.what() << std::endl;
            }
        }

        void deactivate_music_box() {
            try {
                control->mqtt_client->publish(control->mqtt_music_box_id + "/set/activate", "0");
            }
            catch (const mqtt::exception &e) {
                std::cout << "Mqtt exception: " << e.what() << std::endl;
            }
        }

        void set_dmx_channel(int chan, int val) {
            control->dmx_buffer.SetChannel(chan, val);
            update_dmx();
        }

        unsigned get_hazer_duration() {
            return control->hazer_duration;
        }

        void hazer_on() {
            control->dmx_buffer.SetChannel(control->hazer_dmx_channel, 255);
            update_dmx();
        }

        void hazer_off() {
            control->dmx_buffer.SetChannel(control->hazer_dmx_channel, 0);
            update_dmx();
        }

        void set_dmx_scene(const char *dmx_scene) {
            control->dmx_buffer = control->dmx_scenes.at(dmx_scene);
            update_dmx();
        }

        void set_dmx_buffer(const ola::DmxBuffer &dmxBuffer) {
            control->dmx_buffer = dmxBuffer;
            update_dmx();
        }

        void digital_write(int pin, int value) {
            assert(pin >= 0 && unsigned(pin) < N_OUTPUT_PINS);
            control->digio_client.digital_write(pin, value);
        }

        boost::asio::any_io_executor get_io_executor() {
            return control->io_executor;
        }

        std::lock_guard<std::recursive_mutex> create_lock() {
            return std::lock_guard<std::recursive_mutex>(control->m);
        }

    public:
        explicit
        basic_riddle(RoomControl *control)
            : control(control)
            , solved(false)
            , output_pin(-1)
            , output_active(0)
            , solved_time()
        {}

        void set_output_active(int logic_level) override {
            output_active = logic_level;
        }

        bool is_solved() const override {
            return solved;
        }

        void solve(bool by_admin = false) override {
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));
            solved = true;
            admin_solved = by_admin;
            digital_write(output_pin, output_active);
        }

        void reset() override {
            set_solved_timestamp(std::nullopt);
            digital_write(output_pin, !output_active);
            admin_solved = false;
            solved = false;
        }

        int get_output_pin() const override {
            return output_pin;
        }

        void set_output_pin(int pin) override {
            output_pin = pin;
        }

        ~basic_riddle() override {
            reset();
        }

        timestamp_t get_solved_timestamp() const override {
            return solved_time;
        }

        void set_solved_timestamp(const timestamp_t &timestamp) override {
            solved_time = timestamp;
        }

        bool get_admin_solved() const override {
            return admin_solved;
        }

        void set_admin_solved(bool admin_solved) override {
            this->admin_solved = admin_solved;
        }
    };

    class riddle_pregame : public basic_riddle {
    public:
        explicit
        riddle_pregame(RoomControl *control)
            : basic_riddle(control)
        {}

        void solve(bool by_admin = false) override;

        void reset() override;
    };

    class riddle_gamestart : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;

        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_gamestart(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
       {}

        void solve(bool by_admin = false) override;

        void reset() override;

        ~riddle_gamestart() override {
            reset();
        }
    };

    class riddle_light_sound : public basic_riddle {
        const char *dmx_scene;
        const char *sound;

    public:
        explicit
        riddle_light_sound(RoomControl *control,
                           const char *dmx_scene,
                           const char *sound)
            : basic_riddle(control)
            , dmx_scene(dmx_scene)
            , sound(sound)
       {}

        void solve(bool by_admin = false) override {
            set_dmx_scene(dmx_scene);
            start_sound(sound);
            basic_riddle::solve(by_admin);
        }

        void reset() override {
            stop_sound(sound);
            basic_riddle::reset();
        }
    };

    class riddle_four_switches : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_four_switches(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
        {}

        void solve(bool by_admin = false) override {
            namespace dmx = dmx_scene_names;
            namespace snd = sound_names;
            set_dmx_scene(dmx::EMERGENCY);
            start_sound(snd::FOUR_SWITCHES);
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));

            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;

            timer.expires_after(std::chrono::seconds(3));
            timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
                if (e || gen != *gen_ref)
                    return;
                auto l = create_lock();

                set_dmx_scene(dmx::FOUR_SWITCHES);
                digital_write(output_pin, output_active);
            });

            solved = true;
            admin_solved = by_admin;
        }

        void reset() override {
            namespace snd = sound_names;
            (*generation)++;
            timer.cancel();
            digital_write(output_pin, !output_active);
            stop_sound(snd::FOUR_SWITCHES);
            solved = false;
            admin_solved = false;
            set_solved_timestamp(std::nullopt);
        }
    };

    class riddle_drehteile : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_drehteile(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
        {}

        void solve(bool by_admin = false) override {
            namespace dmx = dmx_scene_names;
            namespace snd = sound_names;
            set_dmx_scene(dmx::EMERGENCY);
            start_sound(snd::DREHTEILE);
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));

            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;

            timer.expires_after(std::chrono::seconds(3));
            timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
                if (e || gen != *gen_ref)
                    return;
                auto l = create_lock();

                set_dmx_scene(dmx::DREHTEILE);
                // TODO: Activate TV using MQTT
                digital_write(output_pin, output_active);
            });

            solved = true;
            admin_solved = by_admin;
        }

        void reset() override {
            namespace snd = sound_names;
            (*generation)++;
            timer.cancel();
            digital_write(output_pin, !output_active);
            stop_sound(snd::DREHTEILE);
            solved = false;
            admin_solved = false;
            set_solved_timestamp(std::nullopt);
        }
    };

    /**
     * A single sine wave DMX channel
     */
    class dyn_light_single {
        RoomControl *control;
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer dmx_refresh_timer;
        std::chrono::milliseconds dmx_refresh_interval;
        std::chrono::milliseconds light_cycle;
        std::chrono::milliseconds t;
        int chan;
        int min;
        int max;

        static void dmx_refresh_wrapper(const boost::system::error_code &err, long initial_gen, std::shared_ptr<std::atomic_long> generation, dyn_light_single *self) {
            if (err || initial_gen != *generation)
                return;
            self->dmx_refresh(initial_gen);
        }

        void dmx_refresh(long gen) {
            t += dmx_refresh_interval;
            control->dmx_buffer.SetChannel(chan, min + (std::sin(2 * M_PI * t / light_cycle) + 1) * (max - min) / 2);
            control->update_dmx();
            if (gen == *generation) {
                dmx_refresh_timer.expires_after(dmx_refresh_interval);
                dmx_refresh_timer.async_wait(boost::bind(dyn_light_single::dmx_refresh_wrapper, _1, gen, generation, this));
            }
        }

    public:
        explicit dyn_light_single(RoomControl *control, boost::asio::any_io_executor ioc, int channel, std::chrono::milliseconds light_cycle, int min, int max)
            : control(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , dmx_refresh_timer(ioc)
            , dmx_refresh_interval(25)
            , light_cycle(light_cycle)
            , t(0)
            , chan(channel)
            , min(min)
            , max(max)
        {}

        void start() {
            dmx_refresh(*generation);
        }

        void stop() {
            (*generation)++;
            dmx_refresh_timer.cancel();
        }
    };

    class riddle_candles : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_candles(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
        {}

        void solve(bool by_admin = false) override {
            namespace dmx = dmx_scene_names;
            namespace snd = sound_names;
            set_dmx_scene(dmx::EMERGENCY);
            start_sound(snd::CANDLES);
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));

            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;

            timer.expires_after(std::chrono::seconds(2));
            timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
                if (e || gen != *gen_ref)
                    return;
                auto l = create_lock();

                set_dmx_scene(dmx::CANDLES);
                digital_write(output_pin, output_active);
            });

            solved = true;
            admin_solved = by_admin;
        }

        void reset() override {
            namespace snd = sound_names;
            (*generation)++;
            timer.cancel();
            digital_write(output_pin, !output_active);
            stop_sound(snd::CANDLES);
            solved = false;
            admin_solved = false;
            set_solved_timestamp(std::nullopt);
        }
    };

    class riddle_books : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_books(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
        {}

        void solve(bool by_admin = false) override {
            namespace dmx = dmx_scene_names;
            namespace snd = sound_names;
            set_dmx_scene(dmx::EMERGENCY);
            start_sound(snd::BOOKS);
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));

            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;

            timer.expires_after(std::chrono::seconds(2));
            timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
                if (e || gen != *gen_ref)
                    return;
                auto l = create_lock();

                set_dmx_scene(dmx::BOOKS);
                digital_write(output_pin, output_active);
            });

            solved = true;
            admin_solved = by_admin;
        }

        void reset() override {
            namespace snd = sound_names;
            (*generation)++;
            timer.cancel();
            digital_write(output_pin, !output_active);
            stop_sound(snd::BOOKS);
            solved = false;
            admin_solved = false;
            set_solved_timestamp(std::nullopt);
        }
    };

    class riddle_bible : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;
        boost::asio::steady_timer timer2;

    public:
        explicit
        riddle_bible(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
            , timer2(get_io_executor())
       {}

        void solve(bool by_admin = false) override;

        void reset() override;

        ~riddle_bible() override {
            reset();
        }
    };

    class riddle_time_travel : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;
        boost::asio::steady_timer hazer_timer;

    public:
        explicit
        riddle_time_travel(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
            , hazer_timer(get_io_executor())
       {}

        void solve(bool by_admin = false) override;

        void reset() override;

        ~riddle_time_travel() override {
            reset();
        }
    };

    class riddle_cabinet : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_cabinet(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
       {}

        void solve(bool by_admin = false) override;

        void reset() override;

        ~riddle_cabinet() override {
            reset();
        }
    };

    class riddle_dolls : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_dolls(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
        {}

        void solve(bool by_admin = false) override {
            namespace dmx = dmx_scene_names;
            namespace snd = sound_names;
            set_dmx_scene(dmx::EMERGENCY);
            start_sound(snd::DOLLS);
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));

            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;

            timer.expires_after(std::chrono::seconds(2));
            timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
                if (e || gen != *gen_ref)
                    return;
                auto l = create_lock();

                set_dmx_scene(dmx::DOLLS);
                activate_music_box();
                digital_write(output_pin, output_active);
            });

            solved = true;
            admin_solved = by_admin;
        }

        void reset() override {
            namespace snd = sound_names;
            (*generation)++;
            stop_sound(snd::DOLLS);
            timer.cancel();
            digital_write(output_pin, !output_active);
            deactivate_music_box();
            solved = false;
            admin_solved = false;
            set_solved_timestamp(std::nullopt);
        }
    };

    class riddle_music_box : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer timer;

    public:
        explicit
        riddle_music_box(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , timer(get_io_executor())
        {}

        void solve(bool by_admin = false) override {
            namespace dmx = dmx_scene_names;
            namespace snd = sound_names;
            set_dmx_scene(dmx::EMERGENCY);
            stop_sound(snd::DOLLS);
            deactivate_music_box();
            using std::chrono::system_clock;
            typedef std::chrono::milliseconds millis;
            set_solved_timestamp(date::floor<millis>(system_clock::now()));

            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;

            timer.expires_after(std::chrono::seconds(2));
            timer.async_wait([gen, gen_ref, this] (const boost::system::error_code &e) {
                if (e || gen != *gen_ref)
                    return;
                auto l = create_lock();

                set_dmx_scene(dmx::MUSIC_BOX);
                start_sound(snd::HATCH);
                digital_write(output_pin, output_active);
            });

            solved = true;
            admin_solved = by_admin;
        }

        void reset() override {
            namespace snd = sound_names;
            (*generation)++;
            stop_sound(snd::HATCH);
            digital_write(output_pin, !output_active);
            timer.cancel();
            solved = false;
            admin_solved = false;
            set_solved_timestamp(std::nullopt);
        }
    };

    class riddle_gamewin : public basic_riddle {
        std::shared_ptr<std::atomic_long> generation;
        boost::asio::steady_timer dmx_refresh_timer;
        std::chrono::milliseconds dmx_refresh_interval;
        std::chrono::milliseconds light_cycle;
        std::chrono::milliseconds t;

        static void dmx_refresh_wrapper(const boost::system::error_code &err, long initial_gen, std::shared_ptr<std::atomic_long> generation, riddle_gamewin *self) {
            if (err || initial_gen != *generation)
                return;
            auto l = self->create_lock();
            self->dmx_refresh(initial_gen);
        }

        void dmx_refresh(long gen) {
            ola::DmxBuffer dmxBuffer;
            dmxBuffer.Blackout();
            t += dmx_refresh_interval;
            int len = 4;
            for (int i = 0; i < len; i++) {
                dmxBuffer.SetChannel(19 + i, (std::sin(2 * M_PI * t / light_cycle + 2 * M_PI * i / len) + 1) * 127);
            }
            set_dmx_buffer(dmxBuffer);
            if (gen == *generation) {
                dmx_refresh_timer.expires_after(dmx_refresh_interval);
                dmx_refresh_timer.async_wait(boost::bind(riddle_gamewin::dmx_refresh_wrapper, _1, gen, generation, this));
            }
        }

    public:
        explicit
        riddle_gamewin(RoomControl *control)
            : basic_riddle(control)
            , generation(std::make_shared<std::atomic_long>(0))
            , dmx_refresh_timer(get_io_executor())
            , dmx_refresh_interval(25)
            , light_cycle(4000)
            , t(0)
        {}

        void solve(bool by_admin = false) override {
            using namespace boost::placeholders;
            stop_sound(sound_names::ATMO1);
            stop_sound(sound_names::ATMO2);
            stop_sound(sound_names::ATMO3);
            stop_sound(sound_names::ATMO4);
            long gen = *generation;
            std::shared_ptr<std::atomic_long> gen_ref = generation;
            start_sound(sound_names::GAMEWIN, [this, gen, gen_ref] (int err) {
                if (err != multiaudio::SUCCESS || gen != *gen_ref)
                    return;
                auto l = create_lock();
                (*generation)++;
                dmx_refresh_timer.cancel();
                set_dmx_scene(dmx_scene_names::GAMEWIN);
            });
            basic_riddle::solve(by_admin);
            dmx_refresh(gen);
            control->log_performance();
        }

        void reset() override {
            (*generation)++;
            stop_sound(sound_names::GAMEWIN);
            dmx_refresh_timer.cancel();
            basic_riddle::reset();
            t = std::chrono::milliseconds(0);
            set_solved_timestamp(std::nullopt);
        }
    };

    mutable std::recursive_mutex m;
    bool exiting;

    ola::client::StreamingClient ola_client;
    unsigned ola_universe;
    ola::DmxBuffer dmx_buffer;
    std::map<std::string, ola::DmxBuffer> dmx_scenes;
    unsigned hazer_dmx_channel;
    unsigned hazer_duration;
    std::map<
        std::string /* sound name */,
        std::map<std::string /* language */, multiaudio::async_player /* sound player */>
    > sounds;
    std::map<std::string, std::unique_ptr<iriddle>> riddles;
    std::string input_pin_to_riddle[N_INPUT_PINS];
    digio::client<> digio_client;
    std::optional<mqtt::async_client> mqtt_client;
    std::string mqtt_ghost_tv_id;
    std::string mqtt_music_box_id;
    boost::asio::any_io_executor io_executor;

    dyn_light_single dyn_candle_light;

    char lang[3];

    std::ofstream log_file;

    boost::asio::steady_timer tv_timer;

    int n_group_members;
    int mean_member_age;
    int previous_experience; // 0: none; 1: little; 2: a lot

    // iaction_listener interface
    void on_failure(const mqtt::token &asyncActionToken) override;
    void on_success(const mqtt::token &asyncActionToken) override;

    // callback interface
    void connected(const mqtt::string &) override;
    void connection_lost(const mqtt::string &) override;
    void message_arrived(mqtt::const_message_ptr) override;
    void delivery_complete(mqtt::delivery_token_ptr) override;

    // callback interface
    void on_error(const std::exception &) override;
    void on_init() override;
    void on_input_change(int, int) override;
    void on_output_change(int, int) override;

    // Throw an exception if a required scene does not exist
    void dmx_scenes_check() {
        using namespace dmx_scene_names;

        /** Get a list of all namespace constants as follows:
         *  Replace: ^constexpr char ([[:alpha:]_]+)\[\].*$
         *  With: \1
         */
        for (const std::string &scene : {
             RESETLIGHT,
             PREGAME,
             STARTGAME,
             EMERGENCY,
             FOUR_SWITCHES,
             DREHTEILE,
             TV,
             BIBLE,
             CANDLES,
             BOOKS,
             TIME_TRAVEL,
             CABINET,
             DOLLS,
             MUSIC_BOX,
             GAMEWIN,
        }) {
            if (!dmx_scenes.contains(scene)) {
                throw std::runtime_error("Scene " + scene + " is missing!");
            }
        }
    }

    // Throw an exception if a required sound does not exist
    void sounds_check() {
        using namespace sound_names;
        for (const std::string &sound : {
             STARTGAME,
             SHORT_CIRCUIT,
             ATMO1,
             ATMO2,
             ATMO3,
             ATMO4,
             FOUR_SWITCHES,
             DREHTEILE,
             TV,
             BIBLE,
             CANDLES,
             BOOKS,
             TIME_TRAVEL,
             CABINET,
             DOLLS,
             HATCH,
             GAMEWIN,
    }) {
            if (!sounds.count(sound)) {
                throw std::runtime_error("Sound " + sound + " is missing!");
            }
        }
    }

    void solve(const std::string &riddle, bool admin_solve = false);

    void solve_all();

    void reset(const std::string &riddle);

    void reset_all();

    void publish_all_riddle_states();

    void publish_input_value(int pin, int value);

    void publish_output_value(int pin, int value);

    /* std::string quote(const std::string &str) {
        std::ostringstream ss;
        ss << str;
        return (ss << str).str();
    } */


    void update_dmx() {
        if (!ola_client.SendDmx(ola_universe, dmx_buffer)) {
            std::cout << "Cannot send DMX scene" << std::endl;
        }
    }

    void write_riddle_stats(const iriddle &riddle) {
        iriddle::timestamp_t ts = riddle.get_solved_timestamp();
        if (ts) {
            log_file << riddle.get_admin_solved();
        }
        log_file << ",";
        if (ts) {
            date::to_stream(log_file, TIME_FORMAT, *ts);
        }
    }

    void log_performance() {
        if (log_file.is_open()) {
            const char *const *iter = riddle_names::list;
            assert(*iter);
            write_riddle_stats(*riddles[*iter]);
            while (*(++iter)) {
                log_file << ",";
                write_riddle_stats(*riddles[*iter]);
            }
            log_file << "," << n_group_members << "," << mean_member_age << "," << previous_experience << std::endl;
            if (!log_file) {
                std::cout << "Warning: Cannot write to log file!" << std::endl;
            }
        }
    }

public:
    /*
    RoomControl();

    RoomControl(const RoomControl &r);

    RoomControl(RoomControl &&r);

    void operator =(const RoomControl &r);

    void operator =(RoomControl &&r);
    */

    explicit
    RoomControl(const std::string &mqtt_server_uri,
                const std::string &mqtt_client_id,
                const std::string &mqtt_ghost_tv_id,
                const std::string &mqtt_music_box_id,
                boost::asio::any_io_executor io_executor,
                boost::asio::serial_port &&serial,
                unsigned ola_universe,
                const std::map<std::string, ola::DmxBuffer> &dmx_scenes,
                unsigned hazer_dmx_channel,
                unsigned hazer_duration,
                const std::map<
                    std::string /* sound name */,
                    std::map<std::string /* language */, multisound_pref /* file and channel map */>
                > &sounds,
                const std::map<std::string, riddle_config> &riddle_configs,
                const std::string &log_filename = "")
        : exiting(false)
        , ola_client(ola::client::StreamingClient::Options())
        , ola_universe(ola_universe)
        , dmx_buffer()
        , dmx_scenes(dmx_scenes)
        , hazer_dmx_channel(hazer_dmx_channel)
        , hazer_duration(hazer_duration)
        , input_pin_to_riddle {}
        , digio_client(std::move(serial), this)
        , mqtt_client(std::in_place, mqtt_server_uri, mqtt_client_id)
        , mqtt_ghost_tv_id(mqtt_ghost_tv_id)
        , mqtt_music_box_id(mqtt_music_box_id)
        , io_executor(io_executor)
        , dyn_candle_light(this, io_executor, 2, std::chrono::milliseconds(3000), 120, 200)
        , log_file()
        , tv_timer(io_executor)
        , n_group_members(-1)
        , mean_member_age(-1)
        , previous_experience(-1)
    {
        if (log_filename.size()) {
            bool log_file_exists = std::filesystem::exists(log_filename);

            log_file.open(log_filename, std::ios::app);
            if (!log_file) {
                throw std::runtime_error("Log file cannot be opened!");
            }

            if (!log_file_exists) {
                const char *const *iter = riddle_names::list;
                assert(*iter);
                log_file << *iter << "_adminsolved," << *iter << "_solvetime";
                while (*(++iter)) {
                    log_file << "," << *iter << "_adminsolved," << *iter << "_solvetime";
                }
                log_file << ",number_of_group_members,mean_age,previous_experience" << std::endl;

                if (!log_file) {
                    throw std::runtime_error("Log file header cannot be written!");
                }
            }

            /*
            std::ofstream log_header_file(log_filename + ".header");
            if (!log_header_file) {
                throw std::runtime_error("Log header file cannot be opened for write!");
            }

            const char *const *iter = riddle_names::list;
            assert(*iter);
            log_header_file << *iter << "_adminsolved," << *iter << "_solvetime";
            while (*(++iter)) {
                log_header_file << "," << *iter << "_adminsolved," << *iter << "_solvetime";
            }
            log_header_file << ",number_of_group_members,mean_age,previous_experience" << std::endl;
            log_header_file.close();

            if (!log_header_file) {
                throw std::runtime_error("Log header file cannot be written!");
            }
            */
        }

        strcpy(lang, LANG_DEFAULT);

        dmx_scenes_check();

        using std::unique_ptr;
        using std::make_unique;

        mqtt_client->set_callback(*this);
        dmx_buffer.Blackout();

        for (const auto &[name, lang_prefs]: sounds) {
            for (const auto &[lang, sound_pref]: lang_prefs) {
                this->sounds[name].emplace(lang, multiaudio::async_player(sound_pref.file, sound_pref.map));
            }
        }
        sounds_check();

        typedef std::pair<const std::string, riddle_config> str_riddle_cfg_pair;
        // typedef std::pair<const std::string, unique_ptr<iriddle>> str_riddle_pair;

        namespace rdl = riddle_names;
        namespace snd = sound_names;
        namespace dmx = dmx_scene_names;

        riddles.emplace(rdl::PREGAME, make_unique<riddle_pregame>(this));

        riddles.emplace(rdl::STARTGAME, make_unique<riddle_gamestart>(this));

        riddles.emplace(rdl::FOUR_SWITCHES,
                        make_unique<riddle_four_switches>(this));

        riddles.emplace(rdl::DREHTEILE,
                        make_unique<riddle_drehteile>(this));

        riddles.emplace(rdl::TV,
                        make_unique<riddle_light_sound>(this, dmx::TV, snd::TV));

        riddles.emplace(rdl::BIBLE,
                        make_unique<riddle_bible>(this));

        riddles.emplace(rdl::CANDLES,
                        make_unique<riddle_candles>(this));

        riddles.emplace(rdl::BOOKS,
                        make_unique<riddle_books>(this));

        riddles.emplace(rdl::TIME_TRAVEL, make_unique<riddle_time_travel>(this));

        riddles.emplace(rdl::CABINET, make_unique<riddle_cabinet>(this));

        riddles.emplace(rdl::DOLLS,
                        make_unique<riddle_dolls>(this));

        riddles.emplace(rdl::MUSIC_BOX, make_unique<riddle_music_box>(this));

        riddles.emplace(rdl::GAMEWIN, make_unique<riddle_gamewin>(this));

        for (const str_riddle_cfg_pair &pair: riddle_configs) {
            const std::string &key = pair.first;
            const riddle_config &cfg = pair.second;

            if (riddles.contains(key)) {
                int input_pin = cfg.input_pin;
                int output_pin = cfg.output_pin;
                int output_active = cfg.output_active;
                if (input_pin < 0 || unsigned(input_pin) >= N_INPUT_PINS) {
                    throw std::runtime_error("Input pin out of range!");
                }
                if (output_pin < 0 || unsigned(output_pin) >= N_OUTPUT_PINS) {
                    throw std::runtime_error("Output pin out of range!");
                }

                input_pin_to_riddle[input_pin] = key;
                iriddle *riddle = riddles[key].get();
                riddle->set_output_pin(output_pin);
                riddle->set_output_active(output_active);
            }
            else {
                std::ostringstream ss;
                ss << "No such riddle: " << std::quoted(key);
                throw std::runtime_error(ss.str());
            }
        }

        // Check if all riddles were configured
        for (const auto &[name, riddle] : riddles) {
            int output_pin = riddle->get_output_pin();
            if (output_pin < 0 || unsigned(output_pin) >= N_OUTPUT_PINS) {
                throw std::runtime_error("Riddle " + name + " was not configured!");
            }
        }

        ola_client.SendDmx(ola_universe, dmx_scenes.at(dmx_scene_names::RESETLIGHT));
    }

    /*
    template<
        typename DmxSceneIter,
        typename SoundIter,
        typename RiddleConfigIter,

        typename std::enable_if_t<
                std::is_same<
                    typename std::iterator_traits<DmxSceneIter>::value_type,
                    typename std::pair<const std::string, ola::DmxBuffer>
                >::value
                &&
                std::is_same<
                    typename std::iterator_traits<SoundIter>::value_type,
                    typename std::pair<const std::string, multisound_pref>
                >::value
                &&
                std::is_same<
                    typename std::iterator_traits<RiddleConfigIter>::value_type,
                    typename std::pair<const std::string, riddle_config>
                >::value
            >* = 0
    >
    explicit
    RoomControl(const std::string &mqtt_server_uri,
                const std::string &mqtt_client_id,
                boost::asio::serial_port &&serial,
                unsigned ola_universe,

                DmxSceneIter dmx_scene_begin,
                DmxSceneIter dmx_scene_end,
                SoundIter sound_iter_begin,
                SoundIter sount_iter_end,
                RiddleConfigIter riddle_config_begin,
                RiddleConfigIter riddle_config_end)
        : exiting(false)
        , mqtt_client(mqtt_server_uri, mqtt_client_id)
        , digio_client(std::move(serial),
                       boost::bind(&RoomControl::digio_on_error, this, _1),
                       boost::bind(&RoomControl::digio_on_init, this),
                       boost::bind(&RoomControl::digio_on_pin_change, this, _1, _2))
        , ola_client(ola::client::StreamingClient::Options())
        , ola_universe(ola_universe)
        , dmx_buffer()
        , dmx_scenes(dmx_scene_begin, dmx_scene_end)
    {
        mqtt_client.set_callback(*this);
        dmx_buffer.Blackout();
        dmx_scenes_check();
        typedef std::pair<const std::string, multisound_pref> str_pref_pair;
        typedef std::pair<const std::string, multiaudio::async_player> str_player_pair;
        for (const str_pref_pair &pair: sounds) {
            const std::string &key = pair.first;
            const multisound_pref &pref = pair.second;
            this->sounds.emplace(str_player_pair(key, multiaudio::async_player(pref.file, pref.map)));
        }

        typedef std::pair<const std::string, riddle_config> str_riddle_cfg_pair;
        typedef std::pair<const std::string, unique_ptr<iriddle>> str_riddle_pair;

        using namespace riddle_names;

        riddles.emplace(str_riddle_pair(PREGAME, make_unique<riddle_pregame>(this, 0, this->dmx_scenes[PREGAME])));

        for (const str_riddle_cfg_pair &pair: riddle_configs) {
            const std::string &key = pair.first;
            const riddle_config &cfg = pair.second;
            int input_pin = cfg.input_pin;
            if (input_pin < 0 || unsigned(input_pin) >= N_INPUT_PINS) {
                throw std::runtime_error("Input pin out of range!");
            }

            input_pin_to_riddle[input_pin] = key;
            riddles[key]->set_output_pin(cfg.output_pin);
        }
        sounds_check();

        char str[] = "Hello world";
        size_t len = strlen(str);
        for (int i = 0; i < len, i++) {
            print(str[i]);
        }
        for (char *iter = str; *iter; iter++) {
            printf("%c", *iter);
        }
        for (char *iter = list; *iter; iter = iter->next) {
            printf("%c", *iter);
        }
    }
    */

    ~RoomControl();

    void async_init();
};

}

#endif // ROOMCONTROL_HPP
