#!/bin/sh

# Pass your distribution as the first argument, it can be:
# - Debian_11
# - Raspbian_11

if [ "$#" -lt 1 ]; then
    echo "Distibution expected!" >&2
    exit 1
fi

curl -s "https://build.opensuse.org/projects/home:elaunch/public_key" | gpg --dearmor | sudo tee /usr/share/keyrings/obs-elaunch-archive-keyring.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/obs-elaunch-archive-keyring.gpg] https://download.opensuse.org/repositories/home:/elaunch/$1/ /" | sudo tee /etc/apt/sources.list.d/obs-elaunch.list
